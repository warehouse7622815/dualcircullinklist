#include"DoubleCirculLinkList.h"

int main(int argc, char* argv[])
{
	CDoubleCirculLinkList cLinkList;

	for (int i = 0; i < 3; i++)
	{
		cLinkList.InsertHeadNode(i);
		cLinkList.InsertTailNode(i);
		cLinkList.PrintLinkList();
	}	

	cLinkList.DelHeadNode();
	cLinkList.PrintLinkList();

	cLinkList.DelTailNode();
	cLinkList.PrintLinkList();

	int iData = 0;
	cLinkList.ChangeNodeByData(iData, 9);
	cLinkList.PrintLinkList();

	return 0;
}