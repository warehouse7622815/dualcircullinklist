#include<iostream>
#include"DoubleCirculLinkList.h"
using namespace std;

CDoubleCirculLinkList::CDoubleCirculLinkList()
{
    InitLinkList();
}

CDoubleCirculLinkList::~CDoubleCirculLinkList()
{
    Destroy();
}

/******************************************************************
* @brief InitLinkList          初始化双向循环链表
* Detail                       指定头结点的值,并让其指向自己
*
* @param
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::InitLinkList()
{
    m_pHeadNode = new SDoubleCirculLinkListNode;

    if (!m_pHeadNode)
    {
        cout << "内存分配失败。" << endl;
        exit(1);
    }

    m_pHeadNode->iNodeData = 0;                 
    m_pHeadNode->pNextNode = m_pHeadNode;  //循环链表指向自己    
    m_pHeadNode->pPrevNode = m_pHeadNode;
}

/******************************************************************
* @brief Destroy               销毁链表
* Detail                       释放链表，最后将头结点指向NULL
*
* @param
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::Destroy()
{
    SDoubleCirculLinkListNode* pNode = m_pHeadNode->pNextNode;

    while (pNode != m_pHeadNode)
    {
        SDoubleCirculLinkListNode* pDeletedNode = pNode;
        pNode                                   = pNode->pNextNode;

        delete pDeletedNode;
        pDeletedNode = NULL;
    }

    //释放头结点
    delete pNode;
    pNode = NULL;
    m_pHeadNode   = NULL;
}

/******************************************************************
* @brief InsertHeadNode        头插法
* Detail                       用头插法给链表进行“增”操作
*
* @param[in] iNodeData         插入结点的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::InsertHeadNode(const int iNodeData)
{
    //头结点索引为0
    Insert(0, iNodeData);
}

/******************************************************************
* @brief Insert                按位插入
* Detail                       根据传入的索引iNodeIndex在链表进行
*                              “增”操作
*
* @param[in] iNodeIndex        传入的索引值
* @param[in] iNodeData         插入结点的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::Insert(const int iNodeIndex, const int iNodeData)
{
    SDoubleCirculLinkListNode* pNode = GetNodeByIndex(iNodeIndex);

    if (pNode != NULL)
    {
        SDoubleCirculLinkListNode* pNewNode = new SDoubleCirculLinkListNode;

        if (!pNewNode)
        {
            cout << "内存分配失败。" << endl;
            exit(1);
        }

        pNewNode->iNodeData         = iNodeData;
        pNewNode->pPrevNode         = pNode;
        pNewNode->pNextNode         = pNode->pNextNode;
        pNode->pNextNode->pPrevNode = pNewNode;
        pNode->pNextNode            = pNewNode;

        if (0 == iNodeIndex) 
        {
            cout << "已在头结点后插入元素" << iNodeData << endl;
        }
        else
        {
            cout << "已在第" << iNodeIndex << "元素后插入元素" << iNodeData << endl;
        }
    }
    else
    {
        cout << "未传入有效索引值。" << endl;
    }
}

/******************************************************************
* @brief InsertTailNode        尾插法
* Detail                       用尾插法给链表进行“增”操作
*
* @param[in] iNodeData         插入结点的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::InsertTailNode(const int iNodeData)
{
    Append(iNodeData);
}

/******************************************************************
* @brief Append                在尾部增加元素
* Detail                       用尾部给链表增加元素进行“增”操作
*
* @param[in] iNodeData         插入结点的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::Append(const int iNodeData)
{
    SDoubleCirculLinkListNode* pNewNode = new SDoubleCirculLinkListNode;

    if (!pNewNode)
    {
        cout << "内存分配失败。" << endl;
        exit(1);
    }

    pNewNode->iNodeData               = iNodeData;
    pNewNode->pPrevNode               = m_pHeadNode->pPrevNode;
    pNewNode->pNextNode               = m_pHeadNode;
    m_pHeadNode->pPrevNode->pNextNode = pNewNode;
    m_pHeadNode->pPrevNode            = pNewNode;

    cout << "已在尾部增加元素" << iNodeData << endl;
}

/******************************************************************
* @brief DelHeadNode           头删法
* Detail                       用头删法给链表进行“删”操作
*
* @param
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::DelHeadNode()
{
    //头结点不存储数据，从头结点的下一结点开始删除
    RemoveOneByIndex(1);
}

/******************************************************************
* @brief DelTailNode           尾删法
* Detail                       用尾删法给链表进行“删”操作
*
* @param
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::DelTailNode()
{
    if (Size() > 0)
    {
        RemoveOneByIndex(Size());
    }
    else
    {
        cout << "链表为空，删除失败" << endl;
    }    
}

/******************************************************************
* @brief RemoveOneByIndex      按位删除
* Detail                       根据传入的索引iNodeIndex，删除索引位 
*                              置的结点
*
* @param[in] iNodeIndex        传入的索引值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::RemoveOneByIndex(const int iNodeIndex)
{
    SDoubleCirculLinkListNode* pDeletedNode = GetNodeByIndex(iNodeIndex);

    if (pDeletedNode != NULL)
    {
        pDeletedNode->pPrevNode->pNextNode = pDeletedNode->pNextNode;
        pDeletedNode->pNextNode->pPrevNode = pDeletedNode->pPrevNode;

        delete pDeletedNode;
        pDeletedNode = NULL;

        cout << "已经移除第" << iNodeIndex << "个元素。" << endl;
    }
    else
    {
        cout << "删除失败。" << endl;
    }    
}

/******************************************************************
* @brief RemoveOneByData       按数值删除
* Detail                       根据传入的数值iNodeData获取他在链表
*                              中的索引。如果存在相应结点，按索引删
*                              除该结点
*
* @param[in] iNodeData         传入的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::RemoveOneByData(const int iNodeData)
{
    int iNodeIndex = IndexOf(iNodeData);

    if (iNodeIndex != 0)
    {
        RemoveOneByIndex(iNodeIndex);
        cout << "已经移除元素" << iNodeData << endl;
    }
    else
    {
        cout << "未查询到元素" << iNodeData << endl;
    }
}

/******************************************************************
* @brief FindNodeByData        按值查询链表中的数据
* Detail                       根据传入的数值，查询其索引值，然后调
*                              用Get函数查询该索引下结点
*
* @param[in] iNodeData         需要查询的数值
* @return                      返回查询到的结点
* @note
******************************************************************/
SDoubleCirculLinkListNode* CDoubleCirculLinkList::FindNodeByData(const int iNodeData)
{
    SDoubleCirculLinkListNode* pNode = GetNodeByIndex(IndexOf(iNodeData));
    return pNode;
}

/******************************************************************
* @brief IndexOf               获取元素的索引
* Detail                       根据传入的元素数值，逐结点查询链表。
*                              返回查询到的第一个结点的索引值
*
* @param[in] iNodeData         传入的元素数值
* @return                      返回查询到的索引
* @note
******************************************************************/
int CDoubleCirculLinkList::IndexOf(const int iNodeData)
{    
    SDoubleCirculLinkListNode* pNode = m_pHeadNode->pNextNode;
    int iNodeIndex                   = 0;

    while ((pNode != m_pHeadNode) && (pNode->iNodeData != iNodeData))
    {
        pNode = pNode->pNextNode;
        iNodeIndex++;
    }

    if (pNode != m_pHeadNode)
    {
        cout << iNodeData << "为第" << ++iNodeIndex << "个元素" << endl;
        return iNodeIndex;
    }
    else
    {
        cout << "链表中未查询到相应的值。" << endl;
        return 0;
    }
}

/******************************************************************
* @brief ChangeNodeByData      修改链表中相应的值
* Detail                       调用Update函数实现修改
*
* @param[in] iSrcNodeData      需要修改的结点值
* @param[in] iChangeNodeData   修改后的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::ChangeNodeByData(const int iSrcNodeData, const int iChangeNodeData)
{
    int iNodeIndex = IndexOf(iSrcNodeData);

    if (0 != iNodeIndex)
    {
        Update(iNodeIndex, iChangeNodeData);
        cout << "已将" << iSrcNodeData << "修改为" << iChangeNodeData << endl;
    }
    else
    {
        cout << "修改失败。" << endl;
    }
}

/******************************************************************
* @brief Update                修改链表中相应位置的值
* Detail                       根据传入的索引，查询链表是否有相应的
*                              结点。查询成功将相应索引的结点的数值
*                              修改
*
* @param[in] iSrcNodeIndex     传入的索引值
* @param[in] iChangeNodeData   修改后的数值
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::Update(const int iSrcNodeIndex, const int iChangeNodeData)
{
    SDoubleCirculLinkListNode* pChangedNode = GetNodeByIndex(iSrcNodeIndex);

    if (pChangedNode != NULL)
    {
        pChangedNode->iNodeData = iChangeNodeData;
        cout << "修改成功。" << endl;
    }
    else
    {
        cout << "链表中没有第" << iSrcNodeIndex << "个元素。" << endl;
    }
}

/******************************************************************
* @brief PrintLinkList         打印链表的值
* Detail                       链表为空输出提示，不为空时逐结点输出
*
* @param
* @return
* @note
******************************************************************/
void CDoubleCirculLinkList::PrintLinkList()
{
    if (m_pHeadNode->pNextNode == m_pHeadNode)
    {
        cout << "该链表为空，无值打印" << endl;
    }
    else
    {
        SDoubleCirculLinkListNode* pPrintedNode = m_pHeadNode->pNextNode;

        while (pPrintedNode != m_pHeadNode)
        {
            cout << pPrintedNode->iNodeData << "    ";
            pPrintedNode = pPrintedNode->pNextNode;
        }

        cout << endl;
        cout << endl;
    }
}

/******************************************************************
* @brief GetNodeByIndex        获取对应索引的元素
* Detail                       根据传入的索引，逐结点查询链表。查询
*                              失败返回NULL，查询成功返回查询到的结
*                              点
*
* @param[in] iNodeIndex        传入的索引值
* @return                      返回查询到的结点
* @note
******************************************************************/
SDoubleCirculLinkListNode* CDoubleCirculLinkList::GetNodeByIndex(const int iNodeIndex)
{
    if (iNodeIndex >= 0 && iNodeIndex <= Size())
    {
        SDoubleCirculLinkListNode* pNode = m_pHeadNode;

        for (int i = 0; i < iNodeIndex; i++)
        {
            pNode = pNode->pNextNode;
        }

        return pNode;
    }
    else
    {
        cout << "请传入有效索引值。" << endl;
        return NULL;
    }
}

/******************************************************************
* @brief IsEmpty               判断链表是否为空
* Detail                       链表为空返回true，不为空返回false。
*
* @param
* @return                      返回结果
* @note
******************************************************************/
bool CDoubleCirculLinkList::IsEmpty()
{
    if (m_pHeadNode->pNextNode == m_pHeadNode)
    {
        cout << "该链表为空" << endl;
        return true;
    }
    else
    {
        return false;
    }
}

/******************************************************************
* @brief Size                  计算除头结点外链表结点个数
* Detail                       
*
* @param
* @return                      返回链表结点个数
* @note
******************************************************************/
int CDoubleCirculLinkList::Size()
{
    SDoubleCirculLinkListNode* pNode = m_pHeadNode->pNextNode;
    int iNumOfNode                   = 0;

    while (pNode != m_pHeadNode)
    {
        pNode = pNode->pNextNode;
        iNumOfNode++;
    }

    cout << "链表中有" << iNumOfNode << "个元素" << endl;

    return iNumOfNode;
}
