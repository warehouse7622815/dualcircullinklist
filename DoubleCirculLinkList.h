/******************************************************************
* @file DoubleCirculLinkList.h
* @brief                          对双循环链表实现增删查改
* -----------------------------------------------------------------
* Detail                          对链表实行初始化、打印输出、增
*                                （头插、尾插）、删（头删、尾删）、
*                                 按值查询、修改功能
*------------------------------------------------------------------
* @pversion
* @date       2024/4/7
* @author     张钰琪
* @email      564737727@qq.com
******************************************************************/
#ifndef DOUBLECIRCULLINKLIST_H

typedef struct SDoubleCirculLinkListNode
{
    int                               iNodeData;
    struct SDoubleCirculLinkListNode* pPrevNode;
    struct SDoubleCirculLinkListNode* pNextNode;
}sDoubleCirculLinkListNode, * pDoubleCirculLinkList;

/******************************************************************
* @brief CDoubleCirculLinkList     对双循环链表增删查改
* Detail                           类中有对链表实行初始化、打印输出
*                                  、增（按位插入、尾插）、删（按位
*                                  删除、按值删除）、按值查询、修改
*                                  功能的函数
******************************************************************/
class CDoubleCirculLinkList
{
public:
    CDoubleCirculLinkList();
    ~CDoubleCirculLinkList();
    
    void                       InsertHeadNode(const int iNodeData);
    void                       InsertTailNode(const int iNodeData);
    void                       DelHeadNode(); 
    void                       DelTailNode();
    SDoubleCirculLinkListNode* FindNodeByData(const int iNodeData);
    void                       ChangeNodeByData(const int iSrcNodeData, const int iChangeNodeData);
    void                       PrintLinkList();

private:
    void                       InitLinkList();
    void                       Destroy();
    void                       Append(const int iNodeData);                           
    void                       Insert(const int iNodeIndex, const int iNodeData);       
    void                       RemoveOneByIndex(const int iNodeIndex);                
    void                       RemoveOneByData(const int iNodeData);                
    void                       Update(const int iSrcNodeIndex, const int iChangeNodeData);  
    SDoubleCirculLinkListNode* GetNodeByIndex(const int iNodeIndex);                  
    int                        IndexOf(const int iNodeData);                 
    bool                       IsEmpty();                                         
    int                        Size();                     

private:
    sDoubleCirculLinkListNode* m_pHeadNode;
};

#endif