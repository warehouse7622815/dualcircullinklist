/******************************************************************
* @file       CirculLinkList.h
* @brief                          对带头尾指针的单循环链表增删查改
* -----------------------------------------------------------------
* Detail                          对链表进行初始化、打印输出、增
*                                （头插、尾插）、删（头删、尾删）、
*                                 按值查询、修改功能
*------------------------------------------------------------------
* @pversion
* @date       2024/4/7
* @author     张钰琪
* @email      564737727@qq.com
******************************************************************/
#ifndef CIRCULLINKLIST_H

typedef struct SLinkListNode
{
    int                   iNodeData;
    struct SLinkListNode* pNextNode;
}sLinkListNode, * pLinkList;

/******************************************************************
* @brief CCirculLinkList           对有头尾指针的单循环链表增删查改
* Detail                           类中有对链表实行初始化、打印输
*                                  出、增（头插、尾插）、删（头删、
*                                  尾删）、按值查询、修改功能的函数
******************************************************************/
class CCirculLinkList
{
public:
    CCirculLinkList();
    ~CCirculLinkList();
        
    void           PrintLinkList();
    void           InsertHeadNode(const int iNodeData);
    void           InsertTailNode(const int iNodeData);
    void           DelHeadNode();
    void           DelTailNode();
    SLinkListNode* FindNodeByData(const int iFindData);
    void           ChangeNodeByData(const int iFindNodeData, const int iChangeNodeData);

private:
    void InitLinkList();
    void Destroy();

private:
    sLinkListNode* m_pHeadNode;
    sLinkListNode* m_pTailNode;
};

#endif